import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import axios from 'axios'
import VueAxios from 'vue-axios'
import createPersistedState from 'vuex-persistedstate'
import 'uikit/dist/css/uikit.min.css'
import 'uikit/dist/js/uikit.min.js'

Vue.config.productionTip = false
Vue.use(VueAxios, axios)
Vue.use(Vuex);

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
        countries: []
    },
    actions: {
        addCountry: function ({commit}, country) {
            commit('ADD_COUNTRY', country);
        },
        removeCountry: function ({commit}, country) {
            commit('REMOVE_COUNTRY', country);
        },
        searchByName: function ({commit}, name) {
            return axios.get('https://restcountries.eu/rest/v2/name/' + name);
        },
        searchByCode: function ({commit}, code) {
            return axios.get('https://restcountries.eu/rest/v2/alpha/' + code);
        }
    },
    mutations: {
        ADD_COUNTRY(state, country) {
            state.countries.push(country);
        },
        REMOVE_COUNTRY(state, country) {
            const index = state.countries.findIndex(function (element) {
                return element.alpha3Code === country.alpha3Code;
            });
            state.countries.splice(index, 1);
        }
    },
    getters: {
        countries: function (state) {
            return state.countries;
        },
        inFavorites: function (state) {
            return function (country) {
                return state.countries.find(function (element) {
                    return country.alpha3Code === element.alpha3Code;
                })
            }
        }
    }
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    store,
    components: {App},
    template: '<App/>'
})
